data "gitlab_group" "gitops-demo" {
  full_path = "gitops-demo"
}

resource "gitlab_group" "apps" {
  name             = "apps"
  description      = "My Applications"
  path             = "apps"
  parent_id        = data.gitlab_group.gitops-demo.id
  visibility_level = "public"
}

resource "gitlab_group" "infra" {
  name             = "infra"
  description      = "Infrastructure Projects"
  path             = "infra"
  parent_id        = data.gitlab_group.gitops-demo.id
  visibility_level = "public"
}